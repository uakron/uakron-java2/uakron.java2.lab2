public class PreferredCustomer extends Customer {
	private double purchases;
	private int discount;
	
	public PreferredCustomer(
			String name,
			String address,
			String telephone,
			int customerNumber,
			boolean mailingList,
			double purchases,
			int discount)
	{
		super(name, address, telephone, customerNumber, mailingList);
		this.purchases = purchases;
		this.discount = discount;
	}
	
	public double getPurchases()
	{
		return this.purchases;
	}
	
	public void setPurchases(double purchases)
	{
		this.purchases = purchases;
	}
	
	public int getDiscount()
	{
		return this.discount;
	}
	
	public void setDiscount(int discount)
	{
		this.discount = discount;
	}
	
	// update the discount level depending on the customer's total purchases
	public void updateDiscount()
	{
		if (this.purchases > 2000)
		{
			this.discount = 10;
		}
		else if (this.purchases > 1500)
		{
			this.discount = 7;
		}
		else if (this.purchases > 1000)
		{
			this.discount = 6;
		}
		else if (this.purchases > 500)
		{
			this.discount = 5;
		}
		else
		{
			this.discount = 0;
		}
	}
	
	// optional challenge method replacing demo
	public void updatePurchases(double amount)
	{
		if (amount > 0)
		{
			this.purchases += amount;
			updateDiscount();
		}
	}
}
