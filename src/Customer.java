public class Customer extends Person {
	private int customerNumber;
	private boolean mailingList;
	
	public Customer(
			String name,
			String address,
			String telephone,
			Integer customerNumber,
			boolean mailingList)
	{
		super(name, address, telephone);
		this.customerNumber = customerNumber;
		this.mailingList = mailingList;
	}
	
	public int getCustomerNumber()
	{
		return this.customerNumber;
	}
	
	public void setCustomerNumber(int newCustomerNumber)
	{
		this.customerNumber = newCustomerNumber;
	}
	
	public boolean getMailingList()
	{
		return this.mailingList;
	}
	
	public void setMailingList(boolean newMailingList)
	{
		this.mailingList = newMailingList;
	}
	
	public String numToString()
	{
		return new Integer(customerNumber).toString();
	}
}
