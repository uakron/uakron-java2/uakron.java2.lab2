public class Person {
	private String name;
	private String address;
	private String telephone;
	
	public Person(String name, String address, String telephone)
	{
		this.name = name;
		this.address = address;
		this.telephone = telephone;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String newName)
	{
		this.name = newName;
	}
	
	public String getAddress()
	{
		return this.address;
	}
	
	public void setAddress(String newAddress)
	{
		this.address = newAddress;
	}
	
	public String getTelephone()
	{
		return this.telephone;
	}
	
	public void setTelephone(String newTelephone)
	{
		this.telephone = newTelephone;
	}
}
