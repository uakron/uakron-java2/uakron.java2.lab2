import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class Lab2Tests
{
	@Test
	public void personTest()
	{
		Person person = new Person("Bailey Parrish", "1112 Wall Road", "3302417862");
		person.setName("Riley Miller");
		person.setAddress("132 Third Street");
		person.setTelephone("1234567890");
		
		assertEquals("Riley Miller", person.getName());
		assertEquals("132 Third Street", person.getAddress());
		assertEquals("1234567890", person.getTelephone());
		
		assertNotEquals("Brandon Sweazy", person.getName());
		assertNotEquals("123 Akron Road", person.getAddress());
		assertNotEquals("0987654321", person.getTelephone());
	}
	
	@Test
	public void customerTest()
	{
		Customer customer = new Customer("Bailey Parrish", "1112 Wall Road", "3302417862", 12345, true);
		customer.setCustomerNumber(54321);
		customer.setMailingList(false);
		
		assertEquals(54321, customer.getCustomerNumber());
		assertEquals(false, customer.getMailingList());
		
		assertNotEquals(12345, customer.getCustomerNumber());
		assertNotEquals(true, customer.getMailingList());
	}
	
	@Test
	public void perferredCustomerTest()
	{
		PreferredCustomer preferredCustomer = new PreferredCustomer("Bailey Parrish", "1112 Wall Road", "3302417862", 12345, true, 1000.34, 6);
		preferredCustomer.setPurchases(123.45);
		preferredCustomer.setDiscount(5);
		
		assertEquals(123.45, preferredCustomer.getPurchases());
		assertEquals(5, preferredCustomer.getDiscount());
		
		assertNotEquals(543.21, preferredCustomer.getPurchases());
		assertNotEquals(7, preferredCustomer.getDiscount());
		
		preferredCustomer.updatePurchases(-56.56);
		assertEquals(123.45, preferredCustomer.getPurchases());
		assertNotEquals(-56.56, preferredCustomer.getPurchases());
		
		preferredCustomer.updatePurchases(5432.10);
		assertEquals(123.45 + 5432.10, preferredCustomer.getPurchases());
		assertEquals(10, preferredCustomer.getDiscount());
		assertNotEquals(5432.10, preferredCustomer.getPurchases());
		assertNotEquals(5, preferredCustomer.getDiscount());
	}
}
